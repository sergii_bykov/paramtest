package services;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static services.Constant.*;


public class Calculator extends JFrame{


    private int windowWidth = 300;
    private int windowHeight = 400;

    JLabel number1Label = new JLabel("Первое число:");
    JLabel number2Label = new JLabel("Второе число:");
    JLabel operatorLabel = new JLabel("Оператор:");
    JTextField number1TextField = new JTextField();
    JTextField number2TextField = new JTextField();
    JTextField operatorTextField = new JTextField();
    JTextField outputTextField = new JTextField();
    JButton startButton = new JButton("Вычислить");

    public Calculator(){
        super("Калькулятор");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 3 - windowWidth / 3, dimension.height / 3 - windowHeight / 3, windowWidth, windowHeight);
        Container container = this.getContentPane();
        container.setLayout(new GridLayout(8, 0, 4, 4));

        container.add(number1Label);
        container.add(number1TextField);
        container.add(number2Label);
        container.add(number2TextField);
        container.add(operatorLabel);
        container.add(operatorTextField);
        startButton.addActionListener(new StartButtonListener());
        container.add(startButton);
        container.add(outputTextField);

    }

    public class StartButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event){
            String number1Str = number1TextField.getText();
            String number2Str = number2TextField.getText();
            String operatorStr = operatorTextField.getText();

            String resultStr = "Error";

            if(number1Str.equals(INCORRECT_INPUT) | number2Str.equals(INCORRECT_INPUT) | operatorStr.equals(INCORRECT_INPUT)){
                resultStr = INCORRECT_INPUT;
            }
            else{
                double operand1 = Double.parseDouble(number1Str);
                double operand2 = Double.parseDouble(number2Str);

                Double result;
                Method method = new Method();
                resultStr = String.valueOf(Method.method(operatorStr,operand1,operand2));
                outputTextField.setText(resultStr);

            }
            }
        }
    }


