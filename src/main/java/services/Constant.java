package services;

public class Constant {
    public static final String INCORRECT_INPUT = "Некорректный ввод";
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String DIVIDE = "/";
    public static final String MULTIPLY = "*";
    public static final String REMAINDER_OF_DIVISION = "%";

}
