package services;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MethodTest {
    private static final Method cut = new Method();

    static Arguments[] methodTestArgs(){
        return new Arguments[]{
                Arguments.arguments("6.0","+", 2, 4),
                Arguments.arguments("8.0","*", 2, 4),
                Arguments.arguments("-2.0","-", 2, 4),
                Arguments.arguments("0.5","/", 2, 4),

        };
    }

    @ParameterizedTest
    @MethodSource("methodTestArgs")
    void methodTest(String expected,String operatorStr, double operand1, double operand2) {
        String actual = cut.method(operatorStr,operand1, operand2);

        assertEquals(expected, actual);
    }
}
